#!/bin/bash

# Raspberry Pi - autostart
# v1.0 - 2016-01-10 - start
# v1.1 - 2016-01-14 - dodano uruchomienie midori w privateBrowsing
# v1.2 - 2016-04-10 - logowanie konsolim midori do pliku, logowanie temperatury procesora

# wylaczenie wygaszania monitora
xset -dpms          # disable DPMS (Energy Star) features.  
xset s off          # disable screen saver  
xset s noblank      # don't blank the video device 

# uruchomienie programu do chowania wskaznika myszy
xdotool mousemove 1024 600 &

# uruchomienie programu do przesuwania wskaznika myszy (w rog ekranu)
sudo unclutter -display :0.0 -idle 1 -root &

# przejscie do lokalizacji serwera
cd /home/zzm/kopex/node

# uruchomienie serwera
node forever.js webServer.js &
node forever.js strada.js & # --interval=100 


# logowanie komunikatow z konsoli midori
plik=/home/zzm/kopex/log/midori.log
if [ ! -e $plik ]; then
    touch $plik            # stworzenie pliku
else
    cat /dev/null > $plik  # wyczyszcznie pliku
fi

# rozpoczęcie logowania temperatury procesora
sudo /home/zzm/skryptyBash/raspberry/logTemp.sh &

# uruchomienie przegladarki  
matchbox-window-manager &
midori -e Fullscreen -a /home/zzm/kopex/build/index.html -l /home/zzm/kopex/log/midori.log  -c /home/zzm/.config/midori --private



# ---- OLD ----

# uruchomienie serwera vnc - bez srodowiska X nie da rady!
#sleep 5
#x11vnc -forever &

#sleep 2
#midori -e Fullscreen -a http://127.0.0.1:8888/index.html --private
#midori -e Fullscreen -a /home/zzm/kopex/build/index.html --private



