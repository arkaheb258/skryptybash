# Wstęp   
Skrypty dla Raspberry Pi.  
Za ich pomocą można zainstalować wszystkie potrzebne elementy po świeżej instalacji minibiana jak i skrypty pomocnicze do późniejszej pracy.


# Spis treści  


- [Instalacja systemu od zera ](docs/instalacja.md)  


- [Instalacja systemu od zera - STARA WERSJA](docs/instalacjaOld.md)   


- [Skrypt pomocniczy](docs/rpu.md)  


- [Rejestracja temperatury](docs/logTemp.md)   












