#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
# autor:    Paweł Karp       
# opis:     konfiguracja świeżego systemu po instalacji minibiana
#
# wersje:
#--------------------------------------------------------------------------------------------------------------------------------
# 1.0 / 2016-01-15
#   - start
#--------------------------------------------------------------------------------------------------------------------------------
scriptName=$(basename $BASH_SOURCE) 



#--------------------------------------------------------------------------------------------------------------------------------
# Deklaracje
#--------------------------------------------------------------------------------------------------------------------------------
NET_FILE=/etc/network/interfaces        # sciezka do pliku z ustawieniami adresu IP
HOME_DIR=$(pwd)                         # miejsce gdzie skrypt zostaje wywołany
LOG_FILE=$(pwd)"/output/messages"
cd ..
LIBS_DIR=$(pwd)/libs                    # miejsce umieszczenia bibliotek pomocniczych

ROOT_UID=0                              # Only users with $UID 0 have root privileges
E_XCD=86                                # Can't change directory?
E_NOINPUT=66
E_NOPERM=77


#--------------------------------------------------------------------------------------------------------------------------------
# Załadowanie bibliotek zewnętrznych
#--------------------------------------------------------------------------------------------------------------------------------
clear                                   # wyczyszczenie konsoli
start=`date +%s`                        # liczba sekund od 1970-01-01 00:00:00 UTC
echo $scriptName

cd $LIBS_DIR
if [ `pwd` != "$LIBS_DIR" ]             # Not in LIBS_DIR?
then
    echo "Zla sciezka dla skryptow zewnetrznych: $LIBS_DIR != $(pwd)"
exit $E_XCD
fi 

cd $HOME_DIR                            # powrót do katalogu domowego
source $LIBS_DIR/displayAlert.sh		# podrasowane wyświetlanie komunikatów na konsoli
source $LIBS_DIR/logToFile.sh		    # logowanie do pliku
source $LIBS_DIR/installPackage.sh		# instalacja pakietu debiana za pomocą apt-get
source $LIBS_DIR/logAndDisplay.sh		# 

    
#--------------------------------------------------------------------------------------------------------------------------------
# Sprawdzenie czy skrypt uruchomił użytkownik z uprawnieniami roota
#--------------------------------------------------------------------------------------------------------------------------------
if [ "$UID" -ne "$ROOT_UID" ]; then 
    displayAlert "Uruchom skrypt jako root" "error" "$scriptName"
	exit $E_NOPERM
fi
logToFile "Start instalacji systemu" $LOG_FILE "start"  # Rozpoczecie logowania



    
#--------------------------------------------------------------------------------------------------------------------------------
# Instalacja pakietów
#--------------------------------------------------------------------------------------------------------------------------------
installPackage "sudo" # sprawdzenie czy zostalo zainstalowane sudo
if [ $? -eq 0 ]; then
    logAndDisplay "Uruchomienie aktualizacji bazy pakietow" "ok" "Pakiety" $LOG_FILE
    apt-get update
else
    logAndDisplay "Pomijam apt-get update" "info" "Pakiety" $LOG_FILE
fi

PACKAGES="sudo mc nano xorg xinit x11-xserver-utils matchbox xdotool unclutter htop ttf-liberation ntp ntpdate ncftp x11vnc dos2unix cpufrequtils i2c-tools midori hicolor-icon-theme wireless-regdb iw crda"
for packet in $PACKAGES; do
    installPackage $packet
    case "$?" in
      "0") logAndDisplay "Zainstalowano pakiet - $packet" "ok" "Pakiety" $LOG_FILE ;;
      "1") logAndDisplay "Blad instalacji pakietu - $packet" "error" "Pakiety" $LOG_FILE ;;
      "2") logAndDisplay "Pakiet juz zainstalowany - $packet" "info" "Pakiety" $LOG_FILE ;;
      *) logAndDisplay "Zla odpowiedz skryptu installPackage.sh" "warn" "Pakiety" $LOG_FILE
    esac
done


#--------------------------------------------------------------------------------------------------------------------------------
# Dodanie użytkownika zzm
#--------------------------------------------------------------------------------------------------------------------------------
#useradd -m zzm >/dev/null 2>&1      # stworzenie nowego użytkownika z katalogiem domowym
#if [ $? -eq 9 ]; then               # kod 9 oznacza, że użytkownik już istnieje
#    logAndDisplay "Użytkownik zzm już istnieje" "info" "useradd" $LOG_FILE
#else
#    echo -e 'wihajster1234\nwihajster1234\n' | sudo passwd zzm   # nadanie hasła
#    logAndDisplay "Stworzono użytkownika zzm, hasło: wihajster1234" "ok" "useradd" $LOG_FILE
#fi

## nadanie uprawnień roota - deklaracja tablicy
declare -a arr=("zzm ALL=(ALL:ALL) ALL" 
"zzm ALL=NOPASSWD: /sbin/shutdown" 
"zzm ALL=NOPASSWD: /sbin/passwd" 
"zzm ALL=NOPASSWD: /bin/date" 
"zzm ALL=NOPASSWD: /usr/bin/node")

for i in "${arr[@]}"
do
    grep "$i" /etc/sudoers -R >/dev/null 2>&1  # sprawdzenie czy taki wpis jest juz w pliku sudoers
    if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
        logAndDisplay "Dodano: $i" "ok" "sudoers" $LOG_FILE
        echo "$i" >> /etc/sudoers # zapis do pliku
    else 
        logAndDisplay "Wpis ju istnieje: $i" "info" "sudoers" $LOG_FILE
    fi
done


#--------------------------------------------------------------------------------------------------------------------------------
# Autologin
#--------------------------------------------------------------------------------------------------------------------------------
file=/etc/systemd/system/getty@tty1.service.d/autologin.conf
if [ ! -e "$file" ]; then # plik nie istnieje 
    logAndDisplay "Tworze plik odpowiadający za automatyczne logowanie" "ok" "autologin" $LOG_FILE
    touch $file
    echo "[Service]" >> $file
    echo "ExecStart= " >> $file
    echo "ExecStart=-/sbin/agetty --autologin zzm --noclear %I 38400 linux" >> $file
    systemctl enable getty@tty1.service
else 
    logAndDisplay "Autologin juz ustawiony" "info" "autologin" $LOG_FILE
fi



#--------------------------------------------------------------------------------------------------------------------------------
# Przegranie plików systemowych
#--------------------------------------------------------------------------------------------------------------------------------
mkdir -p /home/zzm/kopex/log

file=/boot/_cmdline.txt
if [ ! -e "$file" ]; then # plik nie istnieje 
    logAndDisplay "Backup i kopia pliku" "ok" "cmdline.txt" $LOG_FILE
    mv /boot/cmdline.txt /boot/_cmdline.txt          # backup
    cp $HOME_DIR/sources/boot/cmdline.txt /boot      # kopia
else 
    logAndDisplay "Plik juz istnieje" "info" "cmdline.txt" $LOG_FILE
fi

file=/boot/_config.txt
if [ ! -e "$file" ]; then # plik nie istnieje 
    logAndDisplay "Backup i kopia pliku" "ok" "config.txt" $LOG_FILE
    mv /boot/config.txt /boot/_config.txt          # backup
    cp $HOME_DIR/sources/boot/config.txt /boot     # kopia
else
    logAndDisplay "Plik juz istnieje" "info" "config.txt" $LOG_FILE
fi


#--------------------------------------------------------------------------------------------------------------------------------
# Przegranie skryptów
#--------------------------------------------------------------------------------------------------------------------------------
file=/home/zzm/autostart.sh
if [ ! -e "$file" ]; then # plik nie istnieje 
    logAndDisplay "Kopia skryptow startowych" "ok" "autostart.sh" $LOG_FILE
    cp $HOME_DIR/sources/autostart.sh /home/zzm/    
    cp $HOME_DIR/sources/mid.sh /home/zzm/   

    chmod -R 777 /home/zzm/autostart.sh # kopiowanie jest z poziomu roota, nadanie praw do plików dla wszystkich użytkowników
    chmod u+x /home/zzm/autostart.sh    # nadanie uprawnień wykonywalności dla skryptu
    chmod -R 777 /home/zzm/mid.sh
    chmod u+x /home/zzm/mid.sh
else 
    logAndDisplay "Skrypty startowe juz przegrane" "info" "autostart.sh" $LOG_FILE
fi


# Ustawienie automatycznego startu przy wstawaniu systemu
plik=/etc/rc.local
grep "sudo xinit" $plik >/dev/null 2>&1  # sprawdzenie czy taki wpis jest juz w pliku
if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
    # zastąpienie tekstu: "exit 0" dwoma tymi linijkami:
    #sudo xinit ./home/zzm/autostart.sh
    #exit 0
    sed -i "16s/exit 0/sudo xinit .\/home\/zzm\/autostart.sh \nexit 0/" $plik  # "16s/....  zmiana w 16 linijce
    logAndDisplay "Ustawiam automatyczne uruchomienie autostart.sh" "ok" "rc.local" $LOG_FILE 
else
    logAndDisplay "Jest juz wpis uruchomienia autostart.sh" "info" "rc.local" $LOG_FILE  
fi

  
#--------------------------------------------------------------------------------------------------------------------------------
# Ustawienie zarządcy procesora
#--------------------------------------------------------------------------------------------------------------------------------
plik=/etc/default/cpufrequtil
if [ ! -e $plik ]; then
    touch $plik            # stworzenie pliku
    echo "GOVERNOR=\"ondemand\"" >> $plik # performance / ondemand
    logAndDisplay "Ustawienie zarzadcy procesora" "ok" "cpufrequtil" $LOG_FILE
else
    logAndDisplay "Zarzadca procesora juz ustawiony" "info" "cpufrequtil" $LOG_FILE
    #cat /dev/null > $plik  # wyczyszcznie pliku
fi



#--------------------------------------------------------------------------------------------------------------------------------
# Instalacja node.js
#--------------------------------------------------------------------------------------------------------------------------------
node -v >/dev/null 2>&1
if [ $? -eq 0 ]; then
    logAndDisplay "Node juz zainstalowane" "info" "node.js" $LOG_FILE
else
    logAndDisplay "Instalacja srodowiska node" "info" "node.js" $LOG_FILE
    cd $HOME_DIR/downloads
    wget http://node-arm.herokuapp.com/node_latest_armhf.deb
    dpkg -i node_latest_armhf.deb
    node -v
    cd $HOME_DIR
fi


#--------------------------------------------------------------------------------------------------------------------------------
# Dodanie modułów rtc
#--------------------------------------------------------------------------------------------------------------------------------
logAndDisplay "Dodanie modulow kernela dla zegara" "info" "RTC" $LOG_FILE
plik=/etc/modules
MODULY="i2c-dev i2c-bcm2708"
for modul in $MODULY; do
    grep $modul $plik -R >/dev/null 2>&1  # sprawdzenie czy taki wpis jest juz w pliku
    if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
        logAndDisplay "Dodano modul: $modul" "ok" "RTC" $LOG_FILE
        echo "$modul" >> $plik # zapis do pliku
    else 
        logAndDisplay "Wpis juz istnieje: $modul" "info" "RTC" $LOG_FILE
    fi
done


#--------------------------------------------------------------------------------------------------------------------------------
# Dodatki
#--------------------------------------------------------------------------------------------------------------------------------
logAndDisplay "Wylaczenie fsck - File System Check" "info" "fsck" $LOG_FILE
tune2fs -c -1 -i 0 /dev/mmcblk0p2 >/dev/null 2>&1 #prevent fsck from running at boot


logAndDisplay "Wylaczenie indeksowania manuala linuxa" "info" "man" $LOG_FILE #disable man indexing:  
plik=/etc/cron.weekly/man-db
grep "#plikZmieniono" /etc/cron.weekly/man-db -R >/dev/null 2>&1 # sprawdzenie czy taki wpis jest juz w pliku
if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
    sed -i "s/set -e/#plikZmieniono \nexit 0 \nset -e/" $plik 
    logAndDisplay "Wyłączono $plik" "ok" "man" $LOG_FILE 
    #sed -i "1i exit 0" /etc/cron.weekly/man-db # wstawienie tekstu "exit 0" w pierwszej linii
else
    logAndDisplay "Juz zmodyfikowany: $plik" "info" "man" $LOG_FILE  
fi

plik=/etc/cron.daily/man-db
grep "#plikZmieniono" /etc/cron.daily/man-db -R >/dev/null 2>&1  # sprawdzenie czy taki wpis jest juz w pliku
if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
    sed -i "s/set -e/#plikZmieniono \nexit 0 \nset -e/" $plik 
    logAndDisplay "Wylaczono $plik" "ok" "man" $LOG_FILE 
    #sed -i "1i exit 0" /etc/cron.daily/man-db
else
    logAndDisplay "Juz zmodyfikowany: $plik" "info" "man" $LOG_FILE  
fi


#--------------------------------------------------------------------------------------------------------------------------------
# rpu.sh - ustawienie widoczności globalnej
#--------------------------------------------------------------------------------------------------------------------------------
# podlinkowanie skryptu z pomocami aby mógł być wywoływany bez potrzeby podawania lokalizacji
if [ ! -e /usr/local/bin/rpu ]; then
    logAndDisplay "Podlinkowanie skryptu do /usr/local/bin" "ok" "rpu.sh" $LOG_FILE
    ln -s /home/zzm/skryptyBash/raspberry/rpu.sh /usr/local/bin/rpu
else
    logAndDisplay "Skrypt ma juz link w /usr/local/bin/rpu" "info" "rpu.sh" $LOG_FILE
fi


#--------------------------------------------------------------------------------------------------------------------------------
# Zmiana Same Origin Policy dla przeglądarki midori
#--------------------------------------------------------------------------------------------------------------------------------
plik=/home/zzm/.config/midori/config
if [ ! -e $plik ]; then
    logAndDisplay "Brak pliku konfiguracyjnego " "error" "midori" $LOG_FILE
    mkdir -p /home/zzm/.config/midori/
    touch $plik            # stworzenie pliku
    echo "[settings]" >> $plik
    echo "enable-universal-access-from-file-uris=true" >> $plik
    chmod -R 777 $plik
    logAndDisplay "Tworze nowy plik konfiguracyjny" "ok" "midori" $LOG_FILE
else # znaleziono plik
    grep "enable-universal-access-from-file-uris=true" $plik -R >/dev/null 2>&1
    if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
        echo "enable-universal-access-from-file-uris=true" >> $plik
        chmod -R 777 $plik
        logAndDisplay "Aktywuje Same Origin Policy dla przegladarki" "ok" "midori" $LOG_FILE
    else
        logAndDisplay "Same Origin Policy aktywowane" "info" "midori" $LOG_FILE  
    fi
fi


#--------------------------------------------------------------------------------------------------------------------------------
# Problem z crda
#--------------------------------------------------------------------------------------------------------------------------------
#pokazują się komunikaty po dmsg: cfg80211 calling crda to update world regulatory domain
#logAndDisplay "Wylaczenie CRDA" "ok" "modprobe" $LOG_FILE 
#apt-get install -y --no-install-recommends wireless-regdb iw crda

# Pozbycie się modułu do wifi
plik=/etc/modprobe.d/fbdev-blacklist.conf
grep "blacklist brcmfmac" $plik -R >/dev/null 2>&1  # sprawdzenie czy taki wpis jest juz w pliku
if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
    echo "blacklist brcmfmac" >> $plik 
    logAndDisplay "Wylaczenie modulu wifi brcmfmac" "ok" "modprobe" $LOG_FILE 
else
    logAndDisplay "Modul brcmfmac juz wylaczony" "info" "modprobe" $LOG_FILE  
fi


#--------------------------------------------------------------------------------------------------------------------------------
# Wyłączenie fsck dla wszystkich partycji
#--------------------------------------------------------------------------------------------------------------------------------
plik=/etc/fstab
grep "0 1" $plik -R >/dev/null 2>&1
if [ $? -eq  1 ]; then #0-znaleziono string; 1-nie znaleziono
    logAndDisplay "Kontroli dyskow wylaczona" "info" "fsck" $LOG_FILE 
else
    logAndDisplay "Wylaczenie kontroli dyskow" "ok" "fsck" $LOG_FILE 
    sed -i "s/0 1/0 0/" /etc/fstab 
    sed -i "s/0 2/0 0/" /etc/fstab  
fi



#--------------------------------------------------------------------------------------------------------------------------------
# Koniec
#--------------------------------------------------------------------------------------------------------------------------------
sudo chmod -R 777 /home/zzm/kopex
echo ""
echo ""
logAndDisplay "Zakonczenie instalacji" "info" "KONIEC" $LOG_FILE
sleep 1s
end=`date +%s`
runtime=$(((end-start)/60))
logAndDisplay "Czas instalacji: $runtime min." "info" "KONIEC" $LOG_FILE
sleep 1s
echo ""
echo ""
logAndDisplay "ZRESTARTUJ SYSTEM! Polecenie: \"reboot\"" "warn" "KONIEC" $LOG_FILE
echo ""
echo ""
#sleep 5s
#reboot



