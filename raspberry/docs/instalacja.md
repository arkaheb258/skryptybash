# Instalacja systemu od zera   
### Instalacja gołego systemu  
Ściągnąć system minibian: [**link**](https://minibianpi.wordpress.com/)  
Zainstalować go na kartę 2GB za pomocą programu Win32DiskImager.  

Stosować kartę 2GB aby mieć jak najmniejszy obraz systemu do kopiowania backupu na serwer dla produkcji.  
Dodatkowo były problemy z przegrywaniem obrazu stworzonego na karcie 8GB na inną kartę 8GB (były niezgodności rzędu kilku KB).  

### Przygotowanie laptopa  
***PuTTY***  
Do zdalnego połączenia używać programu pomocą programu PuTTY.
Można go pobrać np. ze strony [**http://www.chiark.greenend.org.uk/**](http://www.chiark.greenend.org.uk/)  

Uruchomiać program w następującej konfiguracji:  
>Host Name (or IP adress) - **adresIP** / Port: 22  
Connection type - SSH  
OPEN  


***Filezilla***  
Do przegrywania plików użyć klienta filezilli.  
W jego ustawieniach należy zwiększyć czas oczekiwania:  

>Edytuj / Ustawienia / Połączenie / Limit czasu oczekiwania w sekundach: 60  
     
W Menadżerze Stron skonfigurować nowe połączenie z następującymi ustawieniami:  

>Serwer: **adresIP**  
Protokół: SFTP – SSH File Transfer Protocol  
Typ logowania: Normalna  
Użytkownik: zzm  
Hasło: **wihajster1234**  

Uwaga: Podłączenie do serwera może trwać nawet kilkanaście sekund.  


### Ustalenie adresu IP   
Po zainstalowaniu systemu minibian podłączyć go do routera (system ma ustawione dhcp).  
Następnie wejść w ustawienia routera i znaleźć adres IP raspberry. Szukać urządzenia o nazwie *minibian*.  


### Zmiana ustawień dla użytkownika root  
Zalogować się za pomocą putty:  
`login as: root`    
`password: raspberry`   

Wpisywać kolejno komendy:  
`$ passwd`  - Zmienić hasło dla roota na **wihajster1234**  
`$ adduser zzm` - Stworzyć użytkownika **zzm**, nadać hasło **wihajster1234**   
`$ usermod -aG sudo zzm`  - Nadanie uprawnień administratora  
`$ apt-get update`   - Aktualizacja pakietów debiana  
`$ apt-get install git`  
`$ apt-get install sudo`  
`$ reboot`  - restart  


### Uruchomienie skryptu instalacyjnego  
Od tej pory cały czas logujemy się za pomocą putty na konto zzm:  
`login as: zzm`    
`password: wihajster1234`  

Wpisywać kolejno komendy:  
`$ git clone https://bitbucket.org/pawelprak/skryptyBash`  - przegranie skryptów  
`$ cd skryptyBash/raspberry`    
`$ sudo ./rpu.sh -p`  - Powiększenie partycji systemowej do rozmiaru karty SD  
`$ sudo reboot`  
`$ sudo resize2fs /dev/mmcblk0p2`  
`$ df -h` - sprawdzenie wielkości partycji  

Uruchomienie instalacji  
`$ cd skryptyBash/raspberry`  
`$ sudo ./instalacja.sh`  
Po zakończeniu instalacji zrestartować komputer  
  

### Przegranie plików serwera i wizualizacji
Przegrać wizualicję i serwer za pomocą filezilli.   
Ważne aby zrobić to przed odpaleniem skryptu ponieważ będzie on potrzebował ścieżek do katalogu logów.  

Serwer przegrać do katalogu:  
>/home/zzm/kopex  

Wizualizację przegrać do katalogów:  
>/home/zzm/kopex/build   
/home/zzm/kopex/json   
/home/zzm/kopex/jsonDefault   

Nadaś skryptom uprawnienia:  
`cd /home/zzm/kopex/scripts/`  
`chmod +xs *`  


### Kończenie instalacji  
Ustawić adres na statyczny 192.168.3.31:  
`$ sudo rpu -s`  

Podłączyć wyświetlacz po hdmi i sprawdzić czy startuje wizualizacja.  





 







