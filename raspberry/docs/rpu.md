# Raspberry Utils  
Skrypt pomocniczy do obsługi okrojonego systemu.  
Powinien się znajdować w /home/zzm/skryptyBash/raspberry.  
Do działania potrzebuje skryptów zewnętrznych z katalogu /home/zzm/skryptyBash/libs/

Należy go uruchamiać z uprawnieniami roota.  
Wszystkie dostępne opcje można uzyskać za pomocą komendy:  
`sudo rpu -h`  lub  
`sudo rpu -?`  

