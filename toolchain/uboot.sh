#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
# autor:    Paweł Karp       
# opis:     przygotowanie bootloadera Uboot
#
# wersje:
#--------------------------------------------------------------------------------------------------------------------------------
# 1.0 / 2016-01-08
#   - start
#--------------------------------------------------------------------------------------------------------------------------------


scriptName=$(basename $BASH_SOURCE) 
logAndDisplay "Uruchomienie skryptu" "info" "uboot.sh" $LOG_FILE


#--------------------------------------------------------------------------------------------------------------------------------
# optimize build time with 100% CPU usage
#--------------------------------------------------------------------------------------------------------------------------------
CPUS=$(grep -c 'processor' /proc/cpuinfo)
if [ "$USEALLCORES" = "yes" ]; then
	CTHREADS="-j$(($CPUS + $CPUS/2))";
else
	CTHREADS="-j${CPUS}";
fi


#--------------------------------------------------------------------------------------------------------------------------------
# ściągnięcie kodu
#--------------------------------------------------------------------------------------------------------------------------------
if [ "$CLEAN_SOURCES" = "yes" ]; then # czy ściągnąć wszystko od zera?
    logAndDisplay "Czyszczę katalog z źródłami: CLEAN_SOURCES=yes" "warn" $scriptName $LOG_FILE
    rm -rf $SOURCES_DIR/*
fi

temp="U-Boot"
logAndDisplay "Ściągam $temp"  "info" $scriptName $LOG_FILE
downloadGit $WWW_UBOOT $UBOOT_DIR
case "$?" in
  "0") logAndDisplay "Pobrano - $temp" "ok" $scriptName $LOG_FILE ;;
  "1") logAndDisplay "Błąd pobrania plików - $temp" "error" $scriptName $LOG_FILE ;;
  "2") logAndDisplay "Już pobrane - $temp" "ok" $scriptName $LOG_FILE ;;
  *) logAndDisplay "Zła odpowiedź skryptu downloadGit.sh" "warn" $scriptName $LOG_FILE
esac



#--------------------------------------------------------------------------------------------------------------------------------
# Kompilacja
#--------------------------------------------------------------------------------------------------------------------------------
if [ "$COMPILE_UBOOT" = "no" ]; then # czy rozpocząć kompilację?
    logAndDisplay "Pomijam kompilację bootloadera: COMPILE_UBOOT=no" "warn" $scriptName $LOG_FILE

else
    logAndDisplay "Rozpoczynam kompilację..." "info" $scriptName $LOG_FILE
    cd $UBOOT_DIR

    make -s CROSS_COMPILE=arm-linux-gnueabihf- clean
    make -j1 A20-OLinuXino_MICRO_defconfig CROSS_COMPILE=arm-linux-gnueabihf-
    make -j1 CROSS_COMPILE=arm-linux-gnueabihf-

    if [ $? == 0 ]; then 
        logAndDisplay "Kompilacja uboot zakończona" "ok" $scriptName $LOG_FILE
    else
        logAndDisplay "Błąd kompilacji: $? " "error" $scriptName $LOG_FILE
        exit 1
    fi
    
    # spakowanie binarki i przeniesienie archiwum do katalogu output
    mkdir -p $OUTPUT_DIR/u-boot
    cd $UBOOT_DIR
    logAndDisplay "Pakuję do archiwum binarke" "info" $scriptName $LOG_FILE
    tar cPfz $OUTPUT_DIR/u-boot/micro_u-boot.tgz u-boot-sunxi-with-spl.bin

fi



#    logAndDisplay "Kopiuję repozytorium z sunxi" "info" "uboot.sh" $LOG_FILE
#    git clone $WWW_SUNXI $SUNXI_DIR
#    if [ $? -ne 0 ]; then 
#        logAndDisplay "Błąd pobrania plików z $WWW_SUNXI" "error" "uboot.sh" $LOG_FILE
#        rm $SUNXI_DIR # kasuję katalog
#        exit 1
#    else
#        logAndDisplay "Pobrano sunxi" "ok" "uboot.sh" $LOG_FILE
#    fi


#A20-OLinuXino-Micro





















