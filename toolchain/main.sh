#!/bin/bash
#--------------------------------------------------------------------------------------------------------------------------------
# autor:    Paweł Karp       
# opis:     kompilacja obrazu systemu dla olimex A20
#
# wersje:
#--------------------------------------------------------------------------------------------------------------------------------
# 1.0 / 2015-12-18
#   - start
#--------------------------------------------------------------------------------------------------------------------------------




#--------------------------------------------------------------------------------------------------------------------------------
# Konfiguracja zmiennych i katalogów roboczych
#--------------------------------------------------------------------------------------------------------------------------------
HOME_DIR=$(pwd)                         # miejsce gdzie skrypt zostaje wywołany
cd ..
LIBS_DIR=$(pwd)/libs                    # miejsce umieszczenia bibliotek pomocniczych
cd $HOME_DIR
OUTPUT_DIR=~/compileOlimex/output        # lokalizacja plików wyjściowych:                               
SOURCES_DIR=~/compileOlimex/sources      # lokalizacja plików źródłowych potrzebnych do stworzenia systemu
#TOOLCHAIN_DIR=$(pwd)/toolchain          # lokalizacja ze skryptami będącymi elementami kompilacji 
LOG_FILE=$OUTPUT_DIR"/messages" 

ROOT_UID=0                              # Only users with $UID 0 have root privileges
E_XCD=86                                # Can't change directory?
E_NOINPUT=66
E_NOPERM=77

clear                                   # wyczyszczenie konsoli
echo "katalog roboczy: " $HOME_DIR      # 
sleep .5                                # czekaj 0.5 sekundy       


#--------------------------------------------------------------------------------------------------------------------------------
# Załadowanie bibliotek zewnętrznych
#--------------------------------------------------------------------------------------------------------------------------------
cd $LIBS_DIR
if [ `pwd` != "$LIBS_DIR" ]             # Not in LIBS_DIR?
then
    echo "Zła ścieżka dla skryptów zewnętrznych: $LIBS_DIR != $(pwd)"
exit $E_XCD
fi 

cd $HOME_DIR                            # powrót do katalogu domowego
source $LIBS_DIR/displayAlert.sh		# podrasowane wyświetlanie komunikatów na konsoli
source $LIBS_DIR/logToFile.sh		    # logowanie do pliku
source $LIBS_DIR/logAndDisplay.sh		# logowanie do pliku i wyświetlenie w terminalu
source $LIBS_DIR/installPackage.sh		# instalacja pakietu debiana za pomocą apt-get
source $LIBS_DIR/downloadGit.sh		    # ściąganie repozytoriów z gita
displayAlert "Załadowano skrypty pomocnicze" "ok" "compile.sh"



#--------------------------------------------------------------------------------------------------------------------------------
# Sprawdzenie czy skrypt uruchomił użytkownik z uprawnieniami roota
#--------------------------------------------------------------------------------------------------------------------------------
if [ "$UID" -ne "$ROOT_UID" ] 
then 
    displayAlert "Uruchom skrypt jako root" "error" "compile.sh"
	exit $E_NOPERM
fi



#--------------------------------------------------------------------------------------------------------------------------------
# Stworzenie katalogów roboczych
#--------------------------------------------------------------------------------------------------------------------------------
mkdir -p $OUTPUT_DIR -p $SOURCES_DIR
displayAlert "Tworzę Katalogi robocze..." "ok" "compile.sh" 



#--------------------------------------------------------------------------------------------------------------------------------
# START KOMPILACJI i rozpoczęcie logowania do pliku
#--------------------------------------------------------------------------------------------------------------------------------
displayAlert "Start kompilacji" "info" "compile.sh"
source $HOME_DIR/config.sh	
displayAlert "Rozpoczęcie logowania do $LOG_FILE" "ok" "compile.sh"
logToFile "rozpoczęcie logowania" $LOG_FILE "start"



#--------------------------------------------------------------------------------------------------------------------------------
# synchronizacja zegara
#--------------------------------------------------------------------------------------------------------------------------------
if [ "$SYNC_CLOCK" != "no" ]; then # synchronizacja zegara
	#displayAlert "Synching clock" "info" "compile.sh"
    logAndDisplay "Synching clock" "info" "compile.sh" $LOG_FILE
	ntpdate -s time.ijs.si
fi
start=`date +%s` # liczba sekund od 1970-01-01 00:00:00 UTC



#--------------------------------------------------------------------------------------------------------------------------------
# Przygotowanie komputera hosta
#--------------------------------------------------------------------------------------------------------------------------------
#source $HOME_DIR/prepareHost.sh
displayAlert "Sprawdź i zainstaluj wszystko co miało błedy - [ error ]" "warn" "compile.sh"



#--------------------------------------------------------------------------------------------------------------------------------
# Przygotowanie bootloadera Uboot
#--------------------------------------------------------------------------------------------------------------------------------
source $HOME_DIR/uboot.sh



#--------------------------------------------------------------------------------------------------------------------------------
# Przygotowanie jądra
#--------------------------------------------------------------------------------------------------------------------------------
source $HOME_DIR/kernel.sh






















exit
sleep 3
#--------------------------------------------------------------------------------------------------------------------------------
# koniec
#--------------------------------------------------------------------------------------------------------------------------------

end=`date +%s`
runtime=$(((end-start)/60))
#runtime=$(((end-start)))

#displayAlert "Czas kompilacji: $runtime min." "info" "compile.sh" # wyświetlenie czasu trwania skryptu
logAndDisplay "Czas kompilacji: $runtime min." "info" "compile.sh" $LOG_FILE

exit 0








#read -p "wcisnij [ENTER] aby kontynuować"

#echo $?
#if [ $? == $E_NOINPUT ]; then
#    displayAlert "Plik nie istnieje, tworzę nowy" "" "log"
#fi


