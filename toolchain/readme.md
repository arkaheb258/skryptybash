# Opis  
Skrypty pomocnicze  


# Instalacja  
Zainstalować na laptopie wirtualną maszynę Oracle VirtualBox.  
Na VM zainstalować ubuntu 14 lub debiana 8 jessie.  

Już na zainstalowanej maszynie uruchomić terminal i wpisywać komendy:   
`apt-get update`  
`apt-get -y install git`  
`git clone https://bitbucket.org/pawelprak/skryptyBash --depth 1`  
`cp lib/compile.sh .`  
`chmod +x compile.sh`  
`su`  
`./compile.sh`  


# ToDo  
- logToFile - sprawdzenie czy plik istnieje czy nie  


# Źródła  
Przy tworzeniu skryptów korzystałem z:  
http://www.armbian.com/  
https://github.com/igorpecovnik/lib  
https://raymii.org  
https://olimex.wordpress.com  